
class Nb:

    def __init__(self):
        self.is_trained = False

    def train(self, dataset, classes):
        self.total_unique_words = {}
        self.total_docs_positive = 0
        self.total_docs_negative = 0
        self.total_words_positive = 0
        self.total_words_negative = 0
        self.probability_positive = 0
        self.probability_negative = 0
        self.bag_of_words_positive = {}
        self.bag_of_words_negative = {}

        for index, item in enumerate(dataset):
            if (classes[index] == 1):
                self.total_docs_positive += 1
            else:
                self.total_docs_negative += 1

            for word in item:
                import unidecode
                word = unidecode.unidecode(word)
                if word not in self.total_unique_words:
                    self.total_unique_words[word] = 1

                if (classes[index] == 1):  # If class is positive
                    self.total_words_positive += 1
                    if word in self.bag_of_words_positive:
                        self.bag_of_words_positive[word] += 1
                    else:
                        self.bag_of_words_positive[word] = 1
                else:  # If class is negative
                    self.total_words_negative += 1
                    if word in self.bag_of_words_negative:
                        self.bag_of_words_negative[word] += 1
                    else:
                        self.bag_of_words_negative[word] = 1

        self.probability_positive = self.total_docs_positive / \
            (self.total_docs_positive + self.total_docs_negative)
        self.probability_negative = self.total_docs_negative / \
            (self.total_docs_positive + self.total_docs_negative)
        self.is_trained = True

    def test(self, dataset, results):
        x = 0  # Total positive cases in test
        y = 0  # Total negative cases in test
        if (self.is_trained is False):
            return False
        for index, item in enumerate(dataset):
            p_word_pos = 1
            p_word_neg = 1

            for word in item:
                import unidecode
                word = unidecode.unidecode(word)
                # Total count of word in positive and negative. Created new variables in case the word is not found. Then the word coutn is simply 0.
                word_counter_pos = 0
                word_counter_neg = 0
                if word in self.bag_of_words_positive:
                    word_counter_pos = self.bag_of_words_positive[word]
                if word in self.bag_of_words_negative:
                    word_counter_neg = self.bag_of_words_negative[word]

                # Naive Bayes algorithm
                p_word_pos *= float((word_counter_pos + 1) /
                                    (self.total_words_positive + len(self.total_unique_words)))
                p_word_neg *= float((word_counter_neg + 1) /
                                    (self.total_words_negative + len(self.total_unique_words)))

            p_word_pos *= self.probability_positive
            p_word_neg *= self.probability_negative
            print(item)
            print("Probability positive = " + p_word_pos)
            print("Probability negative = " + p_word_neg)
            if p_word_pos > p_word_neg:
                print("Positive comment")
                print("Should be = " + str(results[index]))
                if results[index] == 1:
                    x += 1
                else:
                    y += 1
            else:
                print("Negative comment")
                print("Should be = " + str(results[index]))
                if results[index] == 0:
                    x += 1
                else:
                    y += 1
            print("============")
        print("Correct cases = " + str(x))
        print("Incorrect cases = " + str(y))
        print("Accuracy of: " + str(x/(x+y)*100) + "%")
        return True
